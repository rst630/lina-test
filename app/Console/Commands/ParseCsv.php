<?php

namespace App\Console\Commands;

use App\Models\Customer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class ParseCsv extends Command
{
    protected $result = [];
    protected $input;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Чистим таблицу
        Customer::truncate();

        $data = file(storage_path('app/random.csv'));

        // Собираем заголовок csv
        $head = explode(',',trim($data[0]));

        // Выкидываем строку заголовка
        unset($data[0]);

        foreach ($data as $line)
        {
            $this->input = trim($line);

            // Все поля в строке
            $fields = explode(',',$this->input);

            $insert = [];

            foreach ($head as $i => $col)
            {
                if ($col !== 'location') {
                    $insert[$col] = $fields[$i];
                } else {
                    $location = $fields[$i];

                    // Если не строка или пустая строка то Unknown
                    $insert[$col] = (is_string($location) && !empty($location)) ? $location : 'Unknown';
                }
            }

            // Нормализуем
            $this->normalize($insert);

            // Валидируем
            if ($validator = $this->validate($insert))
            {
                // Если валидация не прошла пишем в отчет и пропускаем
                if ($validator->fails()) {
                    $this->result[] = ['input' => $this->input, 'error' => key($validator->getMessageBag()->getMessages())];
                    continue;
                }
            }

            // Создаем customer
            Customer::create($insert);

        }

        // Выводим отчет
        dump($this->result);

        return 0;
    }

    // Нормализация
    public function normalize(&$data)
    {
        foreach ($data as $key => $val) {
            switch (true) {
                case in_array($key,['name','email','location']):
                    $data[$key] = (string) $val;
                    break;
                case $key == 'age':
                    $data[$key] = (int) $val;
                    break;
            }
        }
    }

    // Валидация
    public function validate($data)
    {
        $validator = Validator::make($data,[
            'name'      => 'required|string',
            'email'     => 'required|email',
            'age'       => 'integer|between:18,99',
            'location'  => 'present|string|nullable'
        ]);

        return $validator;
    }
}
